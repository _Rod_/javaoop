package com.gmail.ryitlearning;

public class Main {

	public static void main(String[] args) {
		// Lesson1 part1

		Cat cat1 = new Cat(12, 5, false, "Milk", "Umka", "Home cat");
		
		System.out.println(cat1.getRation());
		
		System.out.println(cat1.hashCode());
		
		cat1.getVoice();
		
		System.out.println(cat1.toString());
		
		Animal an = cat1;
		
		an.getVoice();
				
		// cat1 = an; 
	}

}
