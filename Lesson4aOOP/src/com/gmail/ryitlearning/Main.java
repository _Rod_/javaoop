package com.gmail.ryitlearning;

import java.util.Arrays;

public class Main {

	public static void main(String[] args) {
		// Sorting of user data array

		Cat cat1 = new Cat("Vaska", 6);
		Cat cat2 = new Cat("Umka", 12);
		Cat cat3 = new Cat("Liska", 4);
		Cat cat4 = new Cat("Barsik", 1);
		Cat cat5 = new Cat("Kuzia", 7);

		Cat[] cats = new Cat[] { cat1, cat2, null, cat3, cat4, null, cat5 };

		for (Cat cat : cats) {
			System.out.println(cat);
		}

		System.out.println();

		Arrays.sort(cats, new CatNameComparator());

		for (Cat cat : cats) {
			System.out.println(cat);
		}
		
		
		System.out.println();
		
		Arrays.sort(cats, new CatAgeComparator());

		for (Cat cat : cats) {
			System.out.println(cat);
		}
		

	}

}
