package com.gmail.ryitlearning;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Constructor;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Cat cat1 = new Cat("Vaska", 5);
		Cat cat2 = new Cat("Vaska", 5);

		System.out.println(cat1.toString());

		System.out.println(cat1.hashCode());
		System.out.println(cat2.hashCode());

		System.out.println(cat1 == cat2);
		System.out.println(cat1.equals(cat2));

		Cat cat3 = null;

		try {
			cat3 = cat1.clone();
		} catch (Exception e) {
			// TODO: handle exception
		}

		System.out
				.println("==========================================================================================");
		System.out.println(cat3 != cat1);

		System.out.println(cat3.getClass() == cat1.getClass());

		System.out.println(cat3.equals(cat1));

		System.out.println("=========================================================================================");
		
		Class catClass = cat1.getClass();
		// System.out.println(catClass);
		Field [] catsFields = catClass.getDeclaredFields();
		for (int i = 0; i < catsFields.length; i++) {
			System.out.println(catsFields[i]);
		}
		System.out.println("=========================================================================================");
		
		Method [] catMethod = catClass.getDeclaredMethods();
		for (int i = 0; i < catMethod.length; i++) {
			System.out.println(catMethod[i]);
		}
		System.out.println("=========================================================================================");
		
		Constructor [] catConstructors = catClass.getDeclaredConstructors();
		for (int i = 0; i < catConstructors.length; i++) {
			System.out.println(catConstructors[i]);
		}
		System.out.println("=========================================================================================");
		
		
		Class [] catInterfaces= catClass.getInterfaces();
		for (int j = 0; j < catInterfaces.length; j++) {
			System.out.println(catInterfaces[j]);
		}
		System.out.println("=========================================================================================");
		
		try {
			Field catAge = catClass.getDeclaredField("age");
			catAge.setAccessible(true);
			catAge.setInt(cat1, 100500);
		} catch (NoSuchFieldException  | SecurityException 
				| IllegalArgumentException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		
		}
		
		System.out.println(cat1);
		
		System.out.println("=========================================================================================");
		
		
		File file = new File("CatFile.txt");
		
		try {
			FileSerializableService.saveToFile(cat1, file);
		} catch (IOException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		Cat cat4 = null;
		
		try {
			cat4 = (Cat) FileSerializableService.loadFromFile(file);
		} catch (ClassNotFoundException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		System.out.println("Cat 4 -> " + cat4);
		
		
		
		
	}

}
