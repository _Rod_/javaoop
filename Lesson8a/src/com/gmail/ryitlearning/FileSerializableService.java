package com.gmail.ryitlearning;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class FileSerializableService {

	public static void saveToFile(Serializable obj, File file) throws IOException {

		try (ObjectOutput ou = new ObjectOutputStream(new FileOutputStream(file))) {
			ou.writeObject(obj);
		} catch (IOException e) {
			throw e;
		}
	}

	public static Serializable loadFromFile(File file) throws IOException, ClassNotFoundException {
		try (ObjectInput oi = new ObjectInputStream(new FileInputStream(file))) {

			return (Serializable) oi.readObject();

		} catch (IOException e) {
			throw e;
		}
	}
}
