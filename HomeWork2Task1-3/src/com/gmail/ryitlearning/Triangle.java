package com.gmail.ryitlearning;

public class Triangle extends Shape {
	private Point point1;
	private Point point2;
	private Point point3;

	public Triangle(Point point1, Point point2, Point point3) {
		super();
		this.point1 = point1;
		this.point2 = point2;
		this.point3 = point3;
	}

	public Triangle() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Point getPoint1() {
		return point1;
	}

	public void setPoint1(Point point1) {
		this.point1 = point1;
	}

	public Point getPoint2() {
		return point2;
	}

	public void setPoint2(Point point2) {
		this.point2 = point2;
	}

	public Point getPoint3() {
		return point3;
	}

	public void setPoint3(Point point3) {
		this.point3 = point3;
	}


	@Override
	public double getArea() {
		// TODO Auto-generated method stub

		double ab = point1.distance(getPoint1(), getPoint2());
		double ac = point2.distance(getPoint1(), getPoint3());
		double bc = point3.distance(getPoint2(), getPoint3());
		double p = (ab + ac + bc) / 2.0;
		return Math.sqrt(p*(p-ab)*(p-ac)*(p-bc));
	}

	@Override
	public double getPerimetr() {
		// TODO Auto-generated method stub
		double ab = point1.distance(getPoint1(), getPoint2());
		double ac = point2.distance(getPoint1(), getPoint3());
		double bc = point3.distance(getPoint2(), getPoint3());
		return ab + ac + bc;
	}
	
	@Override
	public String toString() {
		return "Triangle [point1=" + point1 + ", point2=" + point2 + ", point3=" + point3 + "]";
	}

}
