package com.gmail.ryitlearning;

public class Main {

	public static void main(String[] args) {
		/**
		 * 1. �������� ����������� ����� Shape, � ������� ���� ��� ����������� ������
		 * double getPerimetr() � double getArea().
		 * 
		 * 2. �������� ����� Point, � ������� ���� ��� �������� double x double y.
		 * 
		 * 3. �������� ������, ������� ���������, ��� �������, ��� �������������� ������
		 * (��� ������ ���� ����������� Shape). ��� ���� ��� � �������� ������� ������
		 * ��������� ������ Point.
		 * 
		 */

		Rectangle rectangle = new Rectangle(new Point(7, 12), new Point(35, 4.1), new Point(2.1, 3.7), new Point(3.2, 14));
		System.out.println("The perimetr of requested rectangle is -" + rectangle.getPerimetr());
		System.out.println("The area of requested rectangle is - " + rectangle.getArea());

		Triangle triangle = new Triangle(new Point(1, 1), new Point(10, 1), new Point(10, 4));
		System.out.println("The perimetr of requested triangle is - " + triangle.getPerimetr());
		System.out.println("The area of requested triangle is -" + triangle.getArea());

		Shape circle = new Circle(new Point(10, 2.6), new Point(7, 5.82));
		System.out.println("The circumference of requested circle is" + circle.getPerimetr());
		System.out.println("The area of requested circle is" + circle.getArea());

	}

}
