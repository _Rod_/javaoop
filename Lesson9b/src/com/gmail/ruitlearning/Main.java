package com.gmail.ruitlearning;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;




public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		List<String> list1 = new ArrayList<>();

		list1.add("Hello");
		list1.add("World");

		System.out.println(list1);
		System.out.println("------------------------------------------------");


		list1.add(0, "Cat");

		System.out.println(list1);
		System.out.println("------------------------------------------------");


		String temp = list1.get(1);
		// String temp = list1.get(10);

		System.out.println(temp);
		System.out.println("------------------------------------------------");


		list1.set(1, "Bie");

		System.out.println(list1);
		System.out.println("------------------------------------------------");


		Collections.sort(list1);
		System.out.println(list1);
		System.out.println("------------------------------------------------");


		list1.remove(2);
		System.out.println(list1);
		System.out.println("------------------------------------------------");

		
		for (String element : list1) {
			System.out.println(element);
		}
		System.out.println();
		System.out.println("------------------------------------------------");

		
		Iterator<String> itr = list1.iterator();
		for(;itr.hasNext();) {
			String element = itr.next();
			System.out.println(element);

		}
		System.out.println("------------------------------------------------");

	}

}
