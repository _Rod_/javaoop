package com.gmail.ryitlearning;

public class Song implements Runnable {

	private Service s;
	private String text;
	private String genre;
	private int year;

	public Song(Service s, String text, String genre, int year) {
		super();
		this.s = s;
		this.text = text;
		this.genre = genre;
		this.year = year;
	}

	public Song() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Service getS() {
		return s;
	}

	public void setS(Service s) {
		this.s = s;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	@Override
	public void run() {
		for (int i = 0; i < 5; i++) {

			s.printText(text);

		}

	}

}
