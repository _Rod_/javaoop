package com.gmail.ryitlearning;

public class Service {

	private long[] threadId;
	private int index;

	public synchronized void printText(String text) {

		Thread thr = Thread.currentThread();

		for (; thr.getId() != threadId[index];) {
			try {
				wait();
			} catch (InterruptedException e) {
				//
			}
		}

		System.out.print(text + " ");
		index = (index + 1) % threadId.length;
		notifyAll();
	}

	public void register(Thread... threads) {
		threadId = new long[threads.length];

		for (int i = 0; i < threads.length; i++) {
			threadId[i] = threads[i].getId();
		}

	}
}
