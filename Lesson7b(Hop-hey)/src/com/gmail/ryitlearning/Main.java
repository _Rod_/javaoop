package com.gmail.ryitlearning;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Service s = new Service();
		
		Song s1 = new Song (s, "Hop", "pop", 2020);
		Song s2 = new Song (s, "Hey", "pop", 2020);
		Song s3 = new Song (s, "La La Ley", "pop", 2020);
		
		
		Thread thr1 = new Thread(s1);
		Thread thr2 = new Thread(s2);
		Thread thr3 = new Thread(s3);
		
		s.register(thr1, thr2, thr3);
		
		thr1.start();
		thr2.start();
		thr3.start();
		

	}

}
