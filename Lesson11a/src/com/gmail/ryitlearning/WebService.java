package com.gmail.ryitlearning;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;

public class WebService {

	public static String getStringFromURL(String spec, String code) throws IOException {
		String result = "";
		URL url = new URL(spec);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		InputStream is = connection.getInputStream();
		BufferedReader br = new BufferedReader(new InputStreamReader(is, code));
		String temp = "";
		for (; (temp = br.readLine()) != null;) {
			result += temp + System.lineSeparator();
		}
		return result;
	}

	public static void getFileFromURL(String spec, File folder) throws IOException {
		folder.mkdirs();
		URL url = new URL(spec);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		int n = spec.lastIndexOf("/");
		String fileName = spec.substring(n + 1);
		File file = new File(folder, fileName);

		try (InputStream is = connection.getInputStream(); OutputStream os = new FileOutputStream(file)) {
			byte[] buffer = new byte[10_000_000];
			int byteRead = 0;
			for (; (byteRead = is.read(buffer)) > 0;) {
				os.write(buffer, 0, byteRead);
			}
			
		} catch (IOException e) {
			throw e;
		}

		System.out.println("Get file " + fileName + "done!");
	}
	
	public static Map <String, List<String>> getHeaderFromURL(String spec) throws IOException {
		URL url = new URL(spec);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		return connection.getHeaderFields();
	}

}
