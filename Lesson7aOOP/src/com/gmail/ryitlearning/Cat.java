package com.gmail.ryitlearning;

public class Cat {
	
	private String name;

	public Cat(String name) {
		super();
		this.name = name;
	}

	public Cat() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Cat [name=" + name + "]";
	}
	
	public void jump() {
		System.out.println("Jump");
	}

	public static void voice() {
		System.err.println("Meow");
	}
}
