package com.gmail.ryitlearning;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Main {
	public static void main(String[] args) {

		Map<Integer, String> map = new HashMap<>();

		map.put(1, "one");
		map.put(5, "five");
		map.put(10, "ten");

		System.out.println(map);

		String temp = map.get(1);
		System.out.println(temp);

		temp = map.get(100);
		System.out.println(temp);

		Set<Integer> keys = map.keySet();

		for (Integer key : keys) {
			System.out.println(key + " <=> " + "'" + map.get(key) + "'");
		}

	}
}
