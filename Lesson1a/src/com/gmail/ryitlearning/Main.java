package com.gmail.ryitlearning;

public class Main {

	public static void main(String[] args) {
		// Lesson1a
		
		Car car1 = new Car();
		/**car1.color = "Green";
		car1.weight = 2000;
		car1.year = 1980;
		//car1.velosity = 100500;
		
		
		Car car2 = new Car();
		car2.color = "Red";
		car2.weight = 1000;
		car2.year = 2019;*/
		
		//Car.color = "Blue";
		
		car1.setColor("Green");
		car1.setWeight(2000);
		car1.setYear(1980);
		
		Car car2 = new Car("Red", 1000, 2020);
		
		System.out.println(car1);
		System.out.println(car2);
	}

}
