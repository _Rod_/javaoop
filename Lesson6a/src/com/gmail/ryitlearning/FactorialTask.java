package com.gmail.ryitlearning;

import java.math.BigInteger;

public class FactorialTask implements Runnable {

	private int number;

	public FactorialTask(int number) {
		super();
		this.number = number;
	}

	public BigInteger factorial(int n) {
		BigInteger f = new BigInteger("1");
		for (int i = 1; i <= n; i++) {
			f = f.multiply(new BigInteger(Integer.toString(i)));
		}
		return f;
	}

	@Override
	public void run() {

		Thread thr = Thread.currentThread();
		for (int i = 1; i <= this.number; i++) {
			
			if(thr.isInterrupted()) {
				System.out.println(thr.getName() + " Interrupted");
				return;
			}
			
			System.out.println(thr.getName() + " " + i + "!= " + factorial(i));
		}

	}

}
