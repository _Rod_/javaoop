package com.gmail.ryitlearning;

import javax.swing.JOptionPane;

import com.gmail.ryitlearning.exception.NegativeValueException;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		double a;

		NullPointerException error = new NullPointerException("Hello World!");

		for (;;) {

			try {
				a = Double.valueOf(JOptionPane.showInputDialog("Input double number"));

				if (a < 0) {
					NegativeValueException nve;
					nve = new NegativeValueException("Negtive Value");

					nve.setValue(a);
					throw nve;
				}

				break;

			} catch (NumberFormatException e) {
				JOptionPane.showMessageDialog(null, "Error number format");
				throw error;

			} catch (NullPointerException e) {
				a = 0;
				JOptionPane.showMessageDialog(null, "Cancelled by user");
				break;
			} catch (NegativeValueException e) {
				// TODO: handle exception
				JOptionPane.showMessageDialog(null, "Negative value " + e.getValue());
			}
		}
		System.out.println(a);
	}

}
